FROM python:3.5

RUN mkdir /spinnerStage
WORKDIR /spinnerStage
ADD . /spinnerStage

EXPOSE 8000

RUN pip install -r requirements.txt